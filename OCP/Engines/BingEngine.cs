﻿namespace OCP.Engines
{
    using System;

    public class BingEngine : ISearchEngine
    {
        public string Search(string keyword)
        {
            return $"Search by Bing for {keyword}";
        }
    }
}
