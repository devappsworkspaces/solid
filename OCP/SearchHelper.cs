﻿namespace OCP
{
    public class SearchHelper
    {
        public string Search(string keyword, string engineName)
        {
            var result = string.Empty;
            var searchEngine = EngineFactory.CreateEngine(engineName);

            result = searchEngine.Search(keyword);

            return result;
        }
    }
}
