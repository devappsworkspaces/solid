﻿namespace OCP
{
    using System;

    class Program
    {
        static void Main(string[] args)
        {
            var searchHelper = new SearchHelper();
            var result = searchHelper.Search("SOLID", "Yahoo");

            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}
