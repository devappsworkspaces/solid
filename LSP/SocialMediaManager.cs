﻿namespace LSP
{
    using System.Collections.Generic;

    public class SocialMediaManager
    {
        public List<string> GetAllPosts(List<ISocialMedia> socialMediaServices)
        {
            var result = new List<string>();

            socialMediaServices.ForEach((x) => result.AddRange(x.GetAllPosts()));

            return result;
        }

        public Dictionary<string, bool> ShareAll(List<IShareable> shareableSocialMediaServices)
        {
            var result = new Dictionary<string, bool>();

            shareableSocialMediaServices.ForEach((x) =>
            {
                var socialMediaService = (ISocialMedia)x;

                result.Add(socialMediaService.Name, x.Share());
            });

            return result;
        }
    }
}
