﻿namespace LSP
{
    using System;
    using System.Collections.Generic;

    public class Twitter : ISocialMedia
    {
        public string Name { get; set; } = "Twitter";

        public List<string> GetAllPosts()
        {
            return new List<string>
            {
                "Twitter Post 1", "Twitter Post 2", "Twitter Post 3"
            };
        }
    }
}
