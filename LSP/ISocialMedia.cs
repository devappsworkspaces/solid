﻿namespace LSP
{
    using System.Collections.Generic;

    public interface ISocialMedia
    {
        string Name { get; set; }
        List<string> GetAllPosts();
    }
}
