﻿namespace LSP
{
    public interface IShareable
    {
        bool Share();
    }
}
