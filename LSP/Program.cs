﻿namespace LSP
{
    using System;
    using System.Linq;

    class Program
    {
        static void Main(string[] args)
        {
            var socialMediaServices = SocialMediaProvider.GetSocialMediaServices();
            var shareableMediaServices = SocialMediaProvider.GetShareableSocialMediaServices();
            var socialMediaManager = new SocialMediaManager();

            var result = socialMediaManager.GetAllPosts(socialMediaServices);
            var joinedResult = result.Aggregate((current, next) => current + "\n" + next);

            Console.WriteLine(joinedResult);
            Console.ReadLine();

            var shareResult = socialMediaManager.ShareAll(shareableMediaServices);
            var joinedShareResult = shareResult.Select((x) => x.Key + ":" + x.Value)
                .Aggregate((current, next) => current + "\n" + next);

            Console.WriteLine(joinedShareResult);
            Console.ReadLine();
        }
    }
}
