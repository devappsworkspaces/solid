﻿namespace DIP
{
    public interface ITask
    {
        void Run();
    }
}
