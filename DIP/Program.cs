﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIP
{
    class Program
    {
        static void Main(string[] args)
        {
            var oneTask = new Task();
            var taskManager = new TaskManager();

            taskManager.Run(oneTask);
        }
    }
}
