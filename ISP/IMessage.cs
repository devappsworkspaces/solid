﻿namespace ISP
{
    public interface IMessage 
    {
        string Body { get; set; }
        string Address { get; set; }
        bool Send();
    }
}
