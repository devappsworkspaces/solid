﻿namespace ISP
{
    public interface IEmailService : IMessage
    {
        string Subject { get; set; }
    }
}
