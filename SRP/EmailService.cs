﻿namespace SRP
{
    using System;
    using System.Net.Mail;

    public class EmailService
    {
        public void Validate(string emailAddress)
        {
            if (!emailAddress.Contains("@"))
            {
                throw new Exception("Email adress is not valid");
            }
        }

        public void SendEmail(string emailAddress)
        {
            var smtpClient = new SmtpClient();
            smtpClient.Send("", emailAddress, "", "");
        }
    }
}
